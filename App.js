import React from 'react';
import { Platform, StyleSheet } from 'react-native';
import { Container } from 'native-base';
import { ContextProvider } from './Context';
import { StatusBar } from 'expo-status-bar';

import { AppLoading } from 'expo';
import { Asset } from 'expo-asset';
import * as Font from 'expo-font';
import * as Icon from '@expo/vector-icons';

import AppNavigator from './navigation/AppNavigator';

export default class App extends React.Component {
	state = {
		isLoadingComplete: false,
	};

	render() {
		if (!this.state.isLoadingComplete && !this.props.skipLoadingScreen) {
			return (
				<AppLoading
					startAsync={this._loadResourcesAsync}
					onError={this._handleLoadingError}
					onFinish={this._handleFinishLoading}
				/>
			);
		} else {
			return (
				<Container>
					<ContextProvider>
						<AppNavigator />
					</ContextProvider>
					{Platform.OS === 'ios' || <StatusBar style="light" />}
				</Container>
			);
		}
	}

	_loadResourcesAsync = async () => {
		return Promise.all([
			Font.loadAsync({
				// Icons
				...Icon.FontAwesome.font,
				// ...Icon.Ionicons.font,
				// ...Icon.Foundation.font,
				// Fonts
				'lobster-italic': require('./assets/fonts/LobsterTwo-BoldItalic.ttf'),
				"Roboto": require('native-base/Fonts/Roboto.ttf'),
				"Roboto_medium": require('native-base/Fonts/Roboto_medium.ttf'),
			}),
			Asset.loadAsync([require('./assets/images/robot-dev.png'), require('./assets/images/robot-prod.png')]),
		]);
	};

	_handleLoadingError = error => {
		console.warn(error);
	};

	_handleFinishLoading = () => {
		this.setState({ isLoadingComplete: true });
	};
}
