import React, { createContext, useState, useEffect } from 'react'
import { AsyncStorage } from 'react-native'

import { getCategories } from './services/categories'
import { getPublications, getMyPublications, deletePublication, updatePublication } from './services/publications'

const Context = createContext({})

const ContextProvider = ({children}) => {
    const [query, setQuery] = useState('')
    const [maxDist, setMaxDist] = useState(10)
    const [interests, setInterests] = useState([])
    const [categories, setCategories] = useState([])
    const [publications, setPublications] = useState([])
    const [myPublications, setMyPublications] = useState([])
    const [isLoggedIn, setIsLoggedIn] = useState(false)
    const [loading, setLoading] = useState(false)
    
    const _getCategories = async () => {
        const { data } = await getCategories()
        setCategories(data.data)
    }

    const _getPublications = async () => {
        const { data } = await getPublications({
            skip: 0,
            limit: 9,
            categories: interests,
            maxDistance: maxDist * 1000,
            query
        })
        setPublications(data.data)
    }

    const getMorePublications = async () => {
        try{
			const { data } = await getPublications({
                skip: publications.length,
                limit: 9,
                categories: interests,
                maxDistance: maxDist * 1000,
                query
            })
            setPublications([...publications, ...data.data])
        }
        catch(err){
            console.log(err.message)
            throw err
        }
    }

    const _getMyPublications = async () => {
        try{
			const { data } = await getMyPublications()
			setMyPublications(data.data)
        }
        catch(err){
            console.log(err.message)
            throw err
        }
    }

    const deleteMyPublication = async id => {
        setLoading(true);
        try{
            await deletePublication(id)
            const { data } = await getMyPublications()
            setMyPublications(data.data)
            setLoading(false);
        }
        catch(err){
            console.log(err.message)
            throw err
        }
    }

    const _deletePublication = (id) => {
        setPublications(publications.filter(p => p._id != id))
    }
    
    const updateMyPublication = async id => {
        setLoading(true);
        try{
            await updatePublication(id)
            const { data } = await getMyPublications()
            setMyPublications(data.data)
            setLoading(false);
        }
        catch(err){
            console.log(err.message)
            throw err
        }
    }

    const _setInterests = newInterests => {
        AsyncStorage.setItem('interests', JSON.stringify(newInterests))
        setInterests(newInterests)
    }

    const _setMaxDist = newMaxDist => {
        AsyncStorage.setItem('maxDist', JSON.stringify(newMaxDist))
        setMaxDist(newMaxDist)
    }

    const _getMaxDist = async () => {
        const maxDist = await AsyncStorage.getItem('maxDist')
        maxDist && setMaxDist(JSON.parse(maxDist));
    }

    const _getInterests = async () => {
        const interests = await AsyncStorage.getItem('interests')
        interests && setInterests(JSON.parse(interests))
    }

    const emptyState = () => {
        setInterests([])
        setMaxDist(10)
        setQuery('')
        setPublications([])
        setMyPublications([])
    }

    const initialCharge = async () => {
        await _getInterests()
        await _getMaxDist()
        await _getPublications()
        await _getCategories()
        await _getMyPublications()
    }

    const login = () => setIsLoggedIn(true)
    const logout = () => setIsLoggedIn(false)

    useEffect(() => {
        _getPublications()
    }, [
        interests, 
        maxDist, 
        query
    ])

    useEffect(() => {
        isLoggedIn ? initialCharge() : emptyState()
    }, [isLoggedIn])

    return (
        <Context.Provider value={{
            query,
            loading,
            maxDist,
            interests,
            categories,
            publications,
            myPublications,
            login,
            logout,
            setQuery,
            setLoading,
            deleteMyPublication,
            updateMyPublication,
            getMorePublications,
            setMaxDist: _setMaxDist,
            setInterests: _setInterests,
            deletePublication: _deletePublication,
            resetMyPublications: _getMyPublications,
        }}>
            {children}
        </Context.Provider>
    )
}

export { Context, ContextProvider }