import React, { useContext } from 'react'
import { View, Text, StyleSheet, Slider } from 'react-native'
import MultiSelect from 'react-native-multiple-select';

import { Context } from '../Context' 
import Colors from '../constants/Colors';
import { globalStyles } from '../styles/Global';

const FilterBox = () => {
    const { 
        maxDist, 
        setMaxDist, 
        interests,
        setInterests,
        categories 
    } = useContext(Context);
    
    return (
        <View style={styles.container}>
            <View style={styles.labelContainer}>
                <Text style={styles.label}>Distancia máxima</Text>
                <Text>{maxDist} Km</Text>
            </View>
            <Slider
                style={styles.slider}
                minimumValue={1}
                maximumValue={150}
                value={maxDist}
                onSlidingComplete={value => setMaxDist(Math.round(value))}
                thumbTintColor={Colors.red}
                minimumTrackTintColor={Colors.red}
                maximumTrackTintColor={Colors.black}
            />
            <Text style={styles.label}>Intereses</Text>
            <MultiSelect
                hideTags
                hideSearch
                hideDropdown
                hideSubmitButton
                items={categories}
                selectedItems={interests}
                uniqueKey="value"
                displayKey="name"
                selectText="Categorías"
                searchInputPlaceholderText="Buscar categoría"
                onSelectedItemsChange={setInterests}
                selectedItemTextColor={Colors.red}
                selectedItemIconColor={Colors.red}
                searchInputStyle={globalStyles.textInput}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: { 
        width:'100%', 
        padding: 20,
        position: 'absolute',
        top: 163,
        zIndex: 10,
        backgroundColor: Colors.white
    },
    labelContainer: {
        flexDirection:'row', 
        justifyContent: 'space-between'
    },
    label: {
        fontSize: 16,
        fontWeight: '700',
        marginBottom: 10
    },
    slider: { 
        width: '100%', 
        height: 40,
        color: Colors.red
    }
})

export default FilterBox