import React, { useEffect, useState } from 'react';
import { Text, Button } from 'native-base';
import { FontAwesome } from '@expo/vector-icons'
import * as GoogleSignIn from 'expo-google-sign-in';
import { globalStyles } from '../styles/Global'

const GoogleLogin = () => {
    const [user, setUser] = useState(null)

    useEffect(() => {
        initAsync()
    }, [])

    const initAsync = async () => {
        await GoogleSignIn.initAsync();
        _syncUserWithStateAsync();
    };

    const _syncUserWithStateAsync = async () => {
        const user = await GoogleSignIn.signInSilentlyAsync();
        alert(JSON.stringify(user))
        setUser(user)
    };

    const signOutAsync = async () => {
        await GoogleSignIn.signOutAsync();
        setUser(null);
    };

    const signInAsync = async () => {
        try {
            await GoogleSignIn.askForPlayServicesAsync();
            const { type, user } = await GoogleSignIn.signInAsync();
            if (type === 'success') {
                _syncUserWithStateAsync();
            }
        } catch ({ message }) {
            alert('login: Error:' + message);
        }
    };

    const onPress = () => {
        if (user) {
            signOutAsync();
        } else {
            signInAsync();
        }
    };

    return (
        <Button rounded full style={globalStyles.button} onPress={onPress}>
            <FontAwesome size={20} name="google" style={globalStyles.buttonIcon}/>
            <Text style={globalStyles.buttonText}>Iniciar sesión con Google</Text>
        </Button>
    )
}

export default GoogleLogin