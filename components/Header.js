import React from 'react';
import { Header as NativeBaseHeader, Body, Left, Right, Title } from 'native-base';

const Header = () => {
	return (
		<NativeBaseHeader>
			<Left />
			<Body>
				<Title>trueq</Title>
			</Body>
			<Right />
		</NativeBaseHeader>
	);
};

export default Header;
