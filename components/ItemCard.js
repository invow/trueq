import React, { useState, useContext } from 'react';
import { Image, StyleSheet, Alert } from 'react-native';
import { Card, CardItem, Text, Icon, Button } from 'native-base';

import { Context } from '../Context'
import { like } from '../services/publications'
import Colors from '../constants/Colors';

const ItemCard = ({item, navigation}) =>  {
	const { myPublications, deletePublication } = useContext(Context)
	const { description, image, _id, title } = item

	const handlePress = async () => {
		if (myPublications.length > 0) {
			try{
				const { status, data } = await like(_id)
				if (status === 200){
					if (data.match) {
						Alert.alert(
							"Nuevo Trueq!", 
							"Al autor de la publicación también le gusta uno de tus articulos",
							[
								{
									text: 'Ir',
									onPress: () => navigation.navigate('Matches'),
									closable: true
								}
							]
						)
					}
					deletePublication(_id)
				}
				else
					alert('Error on API call')
			}
			catch(err){
				alert('Error on API call')
			}
		}
		else{
			Alert.alert(
				'No tenés publicaciones', 
				'Debés cargar articulos para poder intercambiarlos', 
				[{
					text: 'Realizar Publicación',
					onPress: () => navigation.navigate('AddProduct'),
					closable: true
				}])
		}
	}

	const confirmReview = () => {
		Alert.alert(
			'Denunciar publicación', 
			'Si viste algo inapropiado en esta publicación podes denunciarla.', 
			[{
				text: 'Denunciar',
				onPress: () => reviewPublication(),
				closable: true
			},
			{
				text: 'Cancelar',
				onPress: () => false,
				closable: true
			}
		])
	}

	const reviewPublication = () => {
		//Fetch > PATCH /users > inReview: true
	}

	const updateReview = async () => {
		const response = await createPublication(formData)
		if (response.status == 201){
			setLoading(false);
			Alert.alert("Publicación creada", "Publicación creada exitosamente")
			setFormData({
				title: '',
				description: '',
				category: undefined,
				image: null
			})
			resetMyPublications()
			props.navigation.navigate('App');
		}
		else if (response.status == 401){
			await AsyncStorage.clear();
			props.navigation.navigate('Auth');
		}
	}
	
	return (
		<Card style={styles.card}>
			<CardItem cardBody style={styles.imageContainer}>
				<Image source={{ uri: image }} style={styles.image} />
				<Button
					style={styles.subMenu}
					onPress={() => confirmReview()}
				>
					<Icon style={styles.subMenuIcon} name="megaphone" />
				</Button>
				<Button onPress={handlePress} rounded style={styles.like}>
					<Icon style={styles.likeIcon} name="heart" />
				</Button>
			</CardItem>
			<CardItem cardBody style={{margin: 5}}>
				<Text style={styles.title}>{title}</Text>
			</CardItem>
			<CardItem cardBody style={{marginHorizontal: 5, marginBottom: 15}}>
				<Text style={styles.description}>{description}</Text>
			</CardItem>
		</Card>
	);
}

const styles = StyleSheet.create({
	card: {
		margin:5,
		marginBottom: 30, 
		padding: 5,
	},
	imageContainer: {
		position: 'relative'
	},
	image: { 
		aspectRatio: 1, 
		width: null, 
		flex: 1 
	},
	like: { 
		height: 62,
		width: 61,
		borderRadius: 32,
		textTransform: 'uppercase', 
		backgroundColor: Colors.white,
		color: Colors.blue,
		position: 'absolute', 
		right: 15,
		bottom: 15
	},
	likeIcon: {
		color: Colors.red,
		fontSize: 35,
		textAlign: 'center',
		textAlignVertical: 'center'
	},
	subMenu: { 
		height: 32,
		width: 52,
		borderRadius: 60,
		textTransform: 'uppercase', 
		backgroundColor: 'rgba(255,255,255,.6)',
		color: Colors.blue,
		position: 'absolute', 
		left: 4,
		top: 5
	},
	subMenuIcon: {
		color: Colors.darkGray,
		fontSize: 21,
		textAlign: 'center',
		textAlignVertical: 'center'
	},
	title: {
		fontSize: 18,
		fontWeight: "700",
		margin:0,
		padding:0
	},
	description: {
		fontSize: 14,
		fontWeight: "normal",
		margin:0,
		padding:0
	},
})

export default ItemCard