import React, { useContext } from 'react';
import { Alert } from 'react-native'
import { Text, Button, Right, Left, Body, ListItem, Icon, Thumbnail } from 'native-base';
import { Context } from '../Context'

const ItemList = ({item}) => {

	const { deleteMyPublication } = useContext(Context)

	const deleteConfirmation = () => {
		Alert.alert(
			'Borrar publicación', 
			'La publicación esta por ser borrada.', 
			[{
				text: 'Borrar',
				onPress: () => deleteMyPublication(item._id),
				closable: true
			},
			{
				text: 'Cancelar',
				onPress: () => false,
				closable: true
			}
		])
	}

	return (
		<ListItem thumbnail>
			<Left>
				<Thumbnail square source={{ uri: item.image }} />
			</Left>
			<Body>
				<Text>{item.title}</Text>
				<Text note numberOfLines={1}>{item.description}</Text>
			</Body>
			<Right>
			<Button transparent onPress={() => deleteConfirmation()}>
					<Text>
						<Icon name="trash"></Icon>
					</Text>
				</Button>
			</Right>
		</ListItem>
	);
}

export default ItemList