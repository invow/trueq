import React from 'react'; 
import { Text, Left, Body, ListItem, Thumbnail } from 'native-base';

const MatchCard = ({item, navigation}) => {

	const handlePress = () => {
		navigation.push('MatchDetail', item)
	}

    return(
        <ListItem thumbnail onPress={handlePress}>
			<Left>
				<Thumbnail source={{ uri: item.image }} />
			</Left>
			<Body>
				<Text>{item.username}</Text>
				<Text note numberOfLines={1}>{item.email}</Text>
			</Body>
		</ListItem>
    )
}

export default MatchCard