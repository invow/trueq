import React from 'react';
import { Picker as NativeBasePicker } from 'native-base';
import data from '../assets/mocked_data/categories.json';

const Header = () => {
	const [selected, setSelected] = React.useState(0);
	const handleOnChange = selected => {
		setSelected(selected);
	};
	return (
		<NativeBasePicker
			note
			mode="dropdown"
			style={{ width: 120 }}
			selectedValue={selected}
			onValueChange={handleOnChange}
		>
			{data.categories &&
				data.categories.map(category => (
					<NativeBasePicker.Item label={category.name} value={category.id} key={category.id} />
				))}
		</NativeBasePicker>
	);
};

export default Header;
