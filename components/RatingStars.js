import React from 'react';
import * as Icon from '@expo/vector-icons';
import { View, Platform } from 'react-native';

import Colors from '../constants/Colors';

const RatingStars = props => {
	return (
		<View style={{ ...props.style, flexDirection: 'row', flexWrap: 'wrap' }}>
			<Icon.Ionicons
				style={{ marginRight: 4 }}
				name={Platform.OS === 'ios' ? 'ios-star' : 'md-star'}
				size={20}
				color={Colors.orange}
			/>
			<Icon.Ionicons
				style={{ marginRight: 4 }}
				name={Platform.OS === 'ios' ? 'ios-star' : 'md-star'}
				size={20}
				color={Colors.orange}
			/>
			<Icon.Ionicons
				style={{ marginRight: 4 }}
				name={Platform.OS === 'ios' ? 'ios-star' : 'md-star'}
				size={20}
				color={Colors.orange}
			/>
			<Icon.Ionicons
				style={{ marginRight: 4 }}
				name={Platform.OS === 'ios' ? 'ios-star' : 'md-star'}
				size={20}
				color={Colors.orange}
			/>
			<Icon.Ionicons name={Platform.OS === 'ios' ? 'ios-star-half' : 'md-star-half'} size={20} color={Colors.orange} />
		</View>
	);
};

export default RatingStars;
