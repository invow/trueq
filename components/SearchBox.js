import React, { useContext, useState } from 'react'
import { func } from 'prop-types'
import { View, StyleSheet, TextInput, TouchableOpacity, Text } from 'react-native'
import { FontAwesome } from '@expo/vector-icons'
import { Icon } from 'native-base'
import Colors from '../constants/Colors'

import { Context } from '../Context'
import { globalStyles } from '../styles/Global'
import FilterBox from './FilterBox'

const SearchBox = () => {
    const { setQuery } = useContext(Context)
    const [value, setValue] = useState('')
    const [showFilter, setShowFilter] = useState(false)
    console.log(value)
    const toggleShowFilter = () => {
        setShowFilter(p => !p)
    }

    return (
        <View>
            <Text style={styles.logo}>
                Trueq
            </Text>
            <View style={styles.searchContainer}>

                <View style={{ 
                    ...globalStyles.textInput,
                    ...styles.formRow
                }}>
                    <View style={styles.formItem}>
                        <TextInput 
                            placeholder="Buscar"
                            keyboardType="web-search"
                            style={styles.searchInput}
                            value={value}
                            onChangeText={setValue}
                            onSubmitEditing={() => setQuery(value)}
                        />
                    </View>
                    <TouchableOpacity 
                        style={styles.iconSearch}
                        onPress={() => setQuery(value)}
                    >
                        <Icon name="search" />
                    </TouchableOpacity>
                </View>
                <TouchableOpacity
                    style={styles.iconFilter}
                    onPress={toggleShowFilter}
                >
                    <FontAwesome size={28} name="sliders" style={styles.configIcon} />
                </TouchableOpacity>
            </View>
            { 
                showFilter && <FilterBox />
            }
        </View>
    )
}

const styles = StyleSheet.create({
    searchContainer: {
        backgroundColor: Colors.white,
        height: 103,
        padding: 10,
        flexDirection: 'row',
        width: '100%',
        paddingTop: 33
    },
    searchInput: {
        width: 100

    },
    formRow: {
        flexDirection: 'row',
        flex:1,
        height: 50,
        marginLeft: 10,
        padding: 10,
    },
    formItem: {
        flex: 1,
        height: 50,
    },
    iconSearch: {
        width: 20,
    },
    iconFilter: {
        padding: 10,
    },
    configIcon: {
        color: Colors.gray
    },
    logo: {
        paddingTop: 33,
		fontSize: 36 ,
		color: Colors.red,
		textAlign: 'center',
		fontFamily: 'lobster-italic',
	},
})

SearchBox.propTypes = {
    performSearch: func
}

export default SearchBox