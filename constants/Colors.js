const primary = '#819582';
const secondary = '#c38044';
const white = '#fff';
const black = '#17453d';
const darkGray = '#757575';
const gray = '#aaa';
const lightGray = '#f2f2f2';
const lightGray2 = '#ccc';
const orange = '#c38044';
const red = '#c35b49';
const otherRed = '#c35b4985';
const green = '#819582'; //
const lightBlue = '#C5CAE9';
const blue = '#2196f3';
const skin = '#d1a590';
const lightSkin = '#e8dace';
const pink = '#d27199';

export default {
	primary,
	secondary,
	orange,
	green,
	white,
	darkGray,
	gray,
	lightGray,
	lightGray2,
	lightBlue,
	black,
	blue,
	red,
	otherRed,
	skin,
	pink,
	lightSkin,
	tabIconDefault: lightGray,
	tabIconSelected: white,
	tabBar: white,
	errorBackground: red,
	errorText: white,
	warningBackground: '#EAEB5E',
	warningText: '#66804',
	noticeBackground: primary,
	noticeText: white,
};
