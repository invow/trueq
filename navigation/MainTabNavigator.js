import React from 'react'
import { Platform } from 'react-native'
import {
	createSwitchNavigator,
	createAppContainer,
} from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import { createBottomTabNavigator } from 'react-navigation-tabs'
import Colors from '../constants/Colors'

import TabBarIcon from '../components/TabBarIcon'
import HomeScreen from '../screens/HomeScreen'
import LoginScreen from '../screens/LoginScreen'
import SignUpScreen from '../screens/SignUpScreen'
import ProfileScreen from '../screens/ProfileScreen'
import MatchesScreen from '../screens/MatchesScreen'
import AddProductScreen from '../screens/AddProductScreen'
import MatchDetailScreen from '../screens/MatchDetailScreen'
import AuthLoadingScreen from '../screens/AuthLoadingScreen'

const AuthStack = createStackNavigator({ Login: LoginScreen })
const SignUpStack = createStackNavigator({ SignUp: SignUpScreen })
const ProfileStack = createStackNavigator({ Profile: ProfileScreen })
const HomeStack = createStackNavigator({ Home: HomeScreen })
const MatchesStack = createStackNavigator({ Matches: MatchesScreen, MatchDetail: MatchDetailScreen})
const AddProductStack = createStackNavigator({ AddProduct: AddProductScreen })

HomeStack.navigationOptions = {
	tabBarIcon: ({ focused }) => <TabBarIcon focused={focused} name={Platform.OS === 'ios' ? `ios-search` : 'md-search'} />
}

AddProductStack.navigationOptions = {
	tabBarIcon: ({ focused }) => <TabBarIcon focused={focused} name={Platform.OS === 'ios' ? 'ios-add' : 'md-add'} />,
}

MatchesStack.navigationOptions = {
	tabBarIcon: ({ focused }) => (
		<TabBarIcon focused={focused} name={Platform.OS === 'ios' ? `ios-search` : 'md-contacts'} />
	),
}

ProfileStack.navigationOptions = {
	tabBarIcon: ({ focused }) => <TabBarIcon focused={focused} name={Platform.OS === 'ios' ? 'ios-contact' : 'md-contact'} />
}

export default createAppContainer(
	createSwitchNavigator(																																																																																																										
		{
			AuthLoading: AuthLoadingScreen,
			App: createBottomTabNavigator(
				{
					HomeStack,
					AddProductStack,
					MatchesStack,
					ProfileStack,
				},
				{
					animationEnabled: true,
					tabBarOptions: {
						showLabel: false,
						showIcon: true,
						style: {
							backgroundColor: Colors.red,
						}
					},
				}
			),
			Auth: AuthStack,
			SignUp: SignUpStack,
		},
		{
			initialRouteName: 'AuthLoading',
		}
	)
)
