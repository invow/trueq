import React, { useState, useContext, useRef } from 'react';
import { Dimensions, StyleSheet, TextInput, Image, View, KeyboardAvoidingView, TouchableOpacity, Alert, ScrollView } from 'react-native';
import { Content, Text, Picker, Form, Spinner} from 'native-base';
import * as ImagePicker from 'expo-image-picker';
import { validateAll } from 'indicative/validator'

import { createPublication } from '../services/publications'
import { globalStyles } from '../styles/Global'
import { Context } from '../Context'
import Colors from '../constants/Colors'

const AddProductScreen = props => {
	const descriptionInput = useRef(null)
	
	const [formData, setFormData] = useState({
			title: '',
			description: '',
			image: null,
			category: undefined
		})

	const { categories, resetMyPublications, setLoading, loading } = useContext(Context)
	
	const uploadFile = async () => {
		const { status } = await ImagePicker.requestCameraRollPermissionsAsync();
        if (status !== 'granted') {
          alert('Necesitamos permisos para que esto funcione :/');
		}
		else{
			let result = await ImagePicker.launchImageLibraryAsync({
				mediaTypes: ImagePicker.MediaTypeOptions.All,
				allowsEditing: true,
				aspect: [1, 1],
				quality: 1,
			});
			
			if (!result.cancelled) {
				handleChange('image', result.uri)
			}
		}
	}

	const handleSubmit = async () => {
		try{
			if (formData.image){
				const field_names = {
					title: "titulo",
					category: "categoría",
					description: "descripción"
				}
				const rules = {
					title: 'required|min:4|max:64',
					category: 'required',
					description: 'required|min:6|max:500'
				}
				const messages = {
					required: field => `El campo ${field_names[field]} es requerido`,
					'title.min': 'El título debe contener al menos 4 caracteres',
					'description.min': 'La descripción debe contener al menos 6 caracteres',
					'max': field => `El campo ${field_names[field]} tiene demasiados caracteres`,
				}
				
				await validateAll(formData, rules, messages)
					
				setLoading(true);
				const response = await createPublication(formData)
				if (response.status == 201){
					setLoading(false);
					Alert.alert("Publicación creada", "Publicación creada exitosamente")
					setFormData({
						title: '',
						description: '',
						category: undefined,
						image: null
					})
					resetMyPublications()
					props.navigation.navigate('App');
				}
				else if (response.status == 401){
					await AsyncStorage.clear();
					props.navigation.navigate('Auth');
				}
				else{
					Alert.alert("Error del servidor", "Ocurrió un error en el servidor :'(")
				}
			}
			else{
				Alert.alert('Verifique los siguientes errores', '• Debes cargar una foto para publicar un producto')
			}
		}
		catch(err){
			console.log(err)
			let body = ""
			Array.isArray(err) && err.map(e => {body += `• ${e.message} \n`})
			Alert.alert("Verifique los siguientes errores", body)		}
	}

	const handleChange = (name, value) => {
		setFormData({
			...formData,
			[name]: value
		});
	}
	
	const onValueChange = value => {
		handleChange('category', value)
	}

	const getImageScale = () => {
		const dimensions = Dimensions.get('window');
		const padding = 30;
		return {
			height: Math.round((dimensions.width - padding) * 3 / 4),
			width: (dimensions.width - padding)
		}
	}

	return (
		<Content contentContainerStyle={styles.content}>
			<KeyboardAvoidingView behavior={Platform.OS === "ios" ? "padding" : null} style={styles.container}>
			{loading ? <Spinner style={styles.spinner} color={Colors.red} /> :
			<>
			<ScrollView>
				<Text style={styles.logo}>
                    Trueq
                </Text>
				<Text style={styles.screenTitle}>Nueva Pubicación</Text>
				<Form>
					<View style={styles.formContainer}>
						{ formData.image && <Image source={{ uri: formData.image }} style={{ height: getImageScale().height, width: getImageScale().width, marginBottom: 15 }} />}
						<TouchableOpacity style={globalStyles.button} full onPress={uploadFile}>
							<Text style={globalStyles.buttonText}>{formData.image ? 'Cambiar' : ''} FOTO</Text>
						</TouchableOpacity>
						<TextInput
							style={ globalStyles.textInput }
							onSubmitEditing={() => descriptionInput.current.focus()}
							returnKeyType="next"
							placeholder="Titulo"
							value={formData.title}
							placeholderTextColor={styles.placeHolderColor.color}
							onChangeText={value => handleChange("title", value)}
						/>
						<Picker
							note={false}
							mode="dropdown"
							style={globalStyles.textInput}
							selectedValue={formData.category}
							onValueChange={onValueChange.bind(this)}
						>
							<Picker.Item value={undefined} label='Categoría' color={styles.placeHolderColor.color} />
							{
								categories.map(item => <Picker.Item key={item._id} label={item.name} value={item.value} />)
							}
						</Picker>
						<TextInput 
							style={{ ...globalStyles.textInput, height: 150, textAlignVertical: 'top' }}
							multiline={true}
							value={formData.description}
							ref={descriptionInput}
							placeholder="Descripcion breve" 
							placeholderTextColor={styles.placeHolderColor.color}
							onChangeText={value => handleChange("description", value)}
						/>
						<TouchableOpacity style={globalStyles.button} full onPress={handleSubmit}>
							<Text style={globalStyles.buttonText}>PUBLICAR</Text>
						</TouchableOpacity>
					</View>
				</Form>
			</ScrollView>
		</>}
		</KeyboardAvoidingView>
		</Content>
	)
}

const styles = StyleSheet.create({
	logo: {
		fontSize: 36 ,
		color: Colors.red,
		textAlign: 'center',
		fontFamily: 'lobster-italic',
		textShadowOffset: {
			width: 1,
			height: 1,
		},
		elevation: 1
	},
	screenTitle: {
		margin: 10,
		fontSize: 15,
		fontWeight: "500",
		color: Colors.gray,
		textAlign: 'center'
	},
	container: {
		flex: 1,
		padding: 15,
		backgroundColor: Colors.white,
		width: '100%',
		paddingTop: 33
	},
	content: {
		flex: 1,
		position: 'relative',
		alignItems: 'center',
	},
	tabContent: {
		flex: 1,
		marginTop: 10,
	},
	img: {
		height: 200,
		margin: 15,
		alignItems: 'center'
	},
	placeHolderColor: {
		color: 'rgba(120,120,120,0.7)',
	},
	formContainer: {
		...globalStyles.formContainer,
		marginTop: 30,
	},
	textCategory:{
		color: 'rgba(120,120,120,0.7)',
	},
	spinner: {
		marginTop: '90%'
	}
});

AddProductScreen.navigationOptions = {
	header: null,
};

export default AddProductScreen