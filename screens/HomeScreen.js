import React, { useContext } from 'react'
import { ScrollView, Text, StyleSheet } from 'react-native'
import { Container } from 'native-base'
import ItemCard from '../components/ItemCard'
import SearchBox from '../components/SearchBox'
import { Context } from '../Context'
import Colors from '../constants/Colors'

const HomeScreen = props => {
	const { publications, getMorePublications } = useContext(Context)

	const isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
		const paddingToBottom = 50
		return layoutMeasurement.height + contentOffset.y >= contentSize.height - paddingToBottom
	}
	
	const handleScroll = ({nativeEvent}) => {
		if (isCloseToBottom(nativeEvent)){
			getMorePublications()
		}
	}

	return (
		<Container style={styles.container}>
			<SearchBox {...props} />
			<ScrollView onScroll={handleScroll} scrollEventThrottle={400}>
				{ 
					publications && publications.length 
						? publications.map((item) => (
							<ItemCard key={item._id} item={item} {...props} />
						))
						:  <Text 
							style={{
								textAlign: 'center',
								margin: 20,
								fontSize: 14,
								color: Colors.gray
							}}
						>
							No hay publicaciones que coincidan con tu busqueda, intentá modificando los filtros.
						</Text>
				}
			</ScrollView>
		</Container>
	);
}

const styles = StyleSheet.create({
	container: {
		backgroundColor: Colors.white,
	},
});


HomeScreen.navigationOptions = {
	header: null
}

export default HomeScreen