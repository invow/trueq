import React, { useState, useEffect, useContext, useRef} from 'react'
import { Animated, View, TextInput, StyleSheet, KeyboardAvoidingView, AsyncStorage, Alert } from 'react-native';
import { Content, Text, Button, Spinner } from 'native-base'
import { globalStyles } from '../styles/Global'
import Colors from '../constants/Colors'
import GoogleLogin from '../components/GoogleLogin'
import { login as loginService } from '../services/auth'
import { Context } from '../Context'

const LoginScreen = ({navigation}) => {
	const [formData, setFormData] = useState({
		email: "",
		password: ""
	})
	const { login, loading, setLoading } = useContext(Context)
	const passwordRef = useRef(null)
	const opacity = useRef(new Animated.Value(0)).current

	const handleSubmit = async () => {
		try{
			setLoading(true)
			const response = await loginService(formData)
			if (response.status == 200){
				login()
				await AsyncStorage.setItem('userToken', response.data.token);
				navigation.navigate('App');
			}
			else if (response.status == 401){
				Alert.alert("Credenciales incorrectas", "El usuario y la contraseña no coinciden")
			}
			else{
				response && response.data && response.data.message && alert(response.data.message)
			}
			setLoading(false)
		}
		catch(err){
			console.log(err)
		}
	}

	const handleChange = (name, value) => {
		setFormData(prevState => {
			return {
				...prevState,
				[name]: value
			}
		})
	}
	
	const opacityAnim = () => {
		Animated.timing(opacity, {
			toValue: 1,
			duration: 200,
			useNativeDriver: true
		}).start()
	}
	
	const signUp = () => {
		navigation.navigate('SignUp')
	}

	useEffect(() => {
		opacityAnim()
	}, [])
	
	return (
		<Content style={styles.content}>
			<KeyboardAvoidingView behavior="padding" style={styles.container}>
			{loading ? <Spinner style={styles.spinner} color={Colors.red} /> :
			<>
			<Text style={styles.logo}>
				Trueq
			</Text>
			<Text style={styles.screenTitle}>Cambiar es fácil.</Text>
			<Animated.View style={{ flex:1, width: '100%', opacity }}>
				<View style={styles.formContainer}>
					<TextInput
						style={ globalStyles.textInput }
						autoCapitalize="none"
						onSubmitEditing={() => passwordRef.current.focus()}
						autoCorrect={false}
						keyboardType="email-address"
						returnKeyType="next"
						placeholder="Email"
						placeholderTextColor={styles.placeHolderColor.color}
						onChangeText={value => handleChange("email", value)}
					/>

					<TextInput
						style={ globalStyles.textInput }
						returnKeyType="go"
						ref={ passwordRef }
						placeholder="Contraseña"
						placeholderTextColor={ styles.placeHolderColor.color }
						secureTextEntry
						onChangeText={value => handleChange("password", value)}
					/>

					<Button rounded full style={globalStyles.button} onPress={handleSubmit}>
						<Text style={globalStyles.buttonText}>Entrar</Text>
					</Button>
					<GoogleLogin />
					<View style={globalStyles.hr}></View>

					<Text style={styles.signIn}>
						<Text onPress={ signUp } style={styles.signInLink}>
							NO TENGO CUENTA
						</Text>
					</Text>
				</View>
			</Animated.View>
			</>}
		</KeyboardAvoidingView>
		</Content>
	)
}

LoginScreen.navigationOptions = {
	header: null,
}

const styles = StyleSheet.create({
	content: {
		backgroundColor: Colors.white,
	},
	container: {
		flex: 1,
		padding: 15,
		width: '100%',
		justifyContent: 'center',
    	alignItems: 'center'
	},
	logo: {
		fontSize: 48,
		paddingLeft: 50,
		paddingRight: 50,
		paddingTop: 50,
		color: Colors.red,
		textAlign: 'center',
		fontFamily: 'lobster-italic',
	},
	screenTitle: {
		margin: 10,
		fontSize: 15,
		fontWeight: "500",
		color: Colors.gray,
		textAlign: 'center'
	},
	signIn: {
		marginTop: 20,
		fontSize: 14,
		color: Colors.gray
	},
	signInLink: {
		fontSize: 14,
		fontWeight: 'bold',
		color: Colors.skin
	},
	placeHolderColor: {
		color: 'rgba(120,120,120,0.7)',
	},
	formContainer: {
		...globalStyles.formContainer,
		marginTop: 30,
	},
	spinner: {
		marginTop: '90%'
	}
});

export default LoginScreen