import React from 'react'
import { FontAwesome } from '@expo/vector-icons'
import { Text, View, StyleSheet, Alert } from 'react-native'
import { Container, Content, Thumbnail, Title, Icon, Button } from 'native-base'
import { updateUser } from '../services/users'
import Colors from '../constants/Colors'

const MatchDetailScreen = ({navigation}) => {
    
    const { username, phone, email, location, image, publications } = navigation.state.params

    const confirmReview = () => {
		Alert.alert(
			'Denunciar usuario', 
			'Si viste algo inapropiado de este usuario podes denunciarlo.', 
			[{
				text: 'Denunciar',
				onPress: () => reviewtUser(),
				closable: true
			},
			{
				text: 'Cancelar',
				onPress: () => false,
				closable: true
			}
		])
    }
    
    const reviewUser = () => {
        //Fetch > PATCH /users > inReview: true
        //updateUser()
    }

    return <Container>
        <Content>
            <View style={{ alignItems: 'center'  }}> 
            <View>
                <Thumbnail medium source={ image && image != '' ? { uri: image } : emptyImage } style={styles.thumbnail} />
            </View>
                <Title style={styles.title}> {username} </Title>
                <Text note style={{ color: Colors.darkGray }} >{location}</Text>
            </View>
            <View style={styles.contactContainer}>
                <View style={styles.contact}>
                    <FontAwesome size={20} name="phone" />
                    <Text style={styles.contactText}>{phone}</Text>
                </View>
                <View style={styles.contact}>
                    <FontAwesome size={20} name="envelope" />
                    <Text style={styles.contactText}>{email}</Text>
                </View>
                <View style={styles.contact}>
                    <Button style={styles.blockBtn} onPress={() => confirmReview()}>
                        <Icon style={styles.blockIcon} size={20} name="megaphone" />
                    </Button>
                </View>
                <View style={styles.contact}>
                    <Button style={styles.blockBtn} onPress={() => confirmReview()}>
                        <Text style={styles.block}>Denunciar</Text>
                    </Button>
                </View>

            </View>
            <View style={styles.publicationsContainer}>
                <Text style={{fontSize: 20, fontWeight: 'bold', paddingVertical: 20}}>
                    Te gustó
                </Text>
                {
                    publications && publications.map(({title, description, image, _id}) => (
                        <View style={styles.publication} key={_id}>
                            <Thumbnail source={{ uri: image }}></Thumbnail>
                            <View style={styles.publicationDetails}>
                                <Text style={{fontWeight:'700'}}>{title}</Text>
                                <Text 
                                    note 
                                    numberOfLines={1} 
                                    style={{ color: Colors.gray}}
                                >
                                    {description}
                                </Text>
                            </View>
                        </View>      
                    ))
                }
            </View>
        </Content>
    </Container>
}

MatchDetailScreen.navigationOptions = {
	header: null
}

const styles = StyleSheet.create({
    thumbnail: {
        height: 120,
        width: 120,
        margin: 40,
        borderRadius: 100
    },
    title: {
        marginTop: 5, 
        color: Colors.black,
        fontSize: 30
        
    },
    contactContainer: {
        marginTop: 20,
        paddingHorizontal: 10
    },
    contact: {
        margin: 5,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    block: {
        margin: 5,
        flexDirection: 'row',
        justifyContent: 'center',
        color: Colors.gray,
        marginBottom: 54
    },
    blockIcon: {
        color: Colors.red
    },
    blockBtn: {
        backgroundColor: 'transparent',
        elevation: 0,
    },
    contactText: {
        fontSize: 18,
        fontWeight: 'bold',
        marginLeft: 10

    },
    publicationsContainer: {
        padding: 20
    },
    publication: {
        flexDirection: 'row'
    },
    publicationDetails:{
        padding: 10
    }

})


export default MatchDetailScreen