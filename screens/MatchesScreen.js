import React, { useState, useEffect } from 'react'
import {StyleSheet, ScrollView, Text} from 'react-native'
import {Container, Content} from 'native-base'

import { getMatches } from '../services/users'
import MatchCard from '../components/MatchCard'
import Colors from '../constants/Colors'

const MatchesScreen = ({navigation}) => {
	const [matches, setMatches]  = useState([])

	const handlePress = () => {
		navigation.push('MatchDetail', {texto: 'datos'})
	}

	const _getMatches = async () => {
		try{
			const { data } = await getMatches()
			setMatches(data.data)
		}
		catch(err){
			alert(err)
		}
	}

	useEffect(() => {
		_getMatches()
	}, [])

	return(
		<Container  style={styles.container}>
			<Content>
				<Text style={styles.logo}>
                    Trueq
                </Text>
				<Text onPress={handlePress} style={styles.screenTitle}>Matches</Text>
				<ScrollView>
					{
						matches.map((item, index) => (
							<MatchCard key={index} item={item} navigation={navigation}/>
						))
					}
				</ScrollView>                            
			</Content>
		</Container>
	)
}

MatchesScreen.navigationOptions = {
	header: null
}

const styles = StyleSheet.create({
	logo: {
		fontSize: 36 ,
		color: Colors.red,
		textAlign: 'center',
		fontFamily: 'lobster-italic',
		textShadowOffset: {
			width: 1,
			height: 1,
		},
		elevation: 1
	},
	screenTitle: {
		margin: 10,
		fontSize: 15,
		fontWeight: "500",
		color: Colors.gray,
		textAlign: 'center'
	},
	container: {
		flex: 1,
		backgroundColor: '#fff',
		padding: 15,
		paddingTop: 33
	}
})

export default MatchesScreen