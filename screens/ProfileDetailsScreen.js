import React, { useState, useContext, useEffect } from 'react'
import * as ImagePicker from 'expo-image-picker'
import { View, Text, StyleSheet, TouchableOpacity, AsyncStorage } from 'react-native'
import { Title, Container, Content, Thumbnail, Button, Header, Icon} from 'native-base'
import Colors from '../constants/Colors'
import { globalStyles } from '../styles/Global'
import { getProfile, uploadImage } from '../services/users'
import { Context } from '../Context'

const emptyImage = require('../assets/images/user-image.jpg')

export default ProfileDetailsScreen = ({navigation}) => {
    const { logout } = useContext(Context)
    const [userData, setUserData] = useState({
        username: '',
        email: '',
        image: null,
        location: ''
    })

    getData = async () => { 
        try{
            const { data } = await getProfile()
            data && setUserData(data)
        }
        catch(err){
            console.log(err.message)
            throw err
        }
    }

    useEffect(() => {
        getData()
    }, [])

    uploadFile = async () => {
        try {

            const { status } = await ImagePicker.requestCameraRollPermissionsAsync();
            if (status !== 'granted') {
              alert('Necesitamos permisos para que esto funcione :/');
            }
            else{
                let result = await ImagePicker.launchImageLibraryAsync({
                    mediaTypes: ImagePicker.MediaTypeOptions.All,
                    allowsEditing: true,
                    aspect: [1, 1],
                    quality: 1,
                });
                const { data } = await uploadImage({image: result.uri})
                setUserData(prevState => {
                    return {
                        ...prevState,
                        image: data.uri
                    }
                })
            }
        }
        catch(err){
            console.log(err)
            throw err
        }
    }
    signOut = async () => {
		await AsyncStorage.clear();
        navigation.navigate('Auth');
        logout()
	};
    return (
        <Container>
            <Content>
                <View style={{ alignItems: 'center'  }}> 
                    <View>
                    <Thumbnail large source={ userData.image && userData.image != '' ? { uri: userData.image } : emptyImage } style={styles.thumbnail} />
                    <TouchableOpacity iconLeft light style={styles.buttonIcon} onPress={uploadFile}> 
                        <Icon type="FontAwesome" name="camera"style={styles.icon} />
                    </TouchableOpacity>
                </View>
                    <Title style={styles.title}> {userData.username} </Title>
                    <Text note style={{ color: Colors.darkGray }} >{userData.location}</Text>
                </View>
                <View style={styles.formContainer}>
                    <TouchableOpacity style={globalStyles.textInput} full> 
                        <Text style={styles.buttonText}>Acerca de Trueq</Text>
                    </TouchableOpacity>
                    
                    <TouchableOpacity style={globalStyles.textInput} full> 
                        <Text style={styles.buttonText}>Ayuda</Text>
                    </TouchableOpacity>
                    
                    <TouchableOpacity style={globalStyles.button} onPress={signOut}> 
                        <Text style={globalStyles.buttonText}>Cerrar sesión</Text>
                    </TouchableOpacity>
                </View>
            </Content>
        </Container>
    )
}

const styles = StyleSheet.create({
    thumbnail: {
        height: 200,
        width: 200,
        margin: 40,
        borderRadius: 100
    },
    title: {
        marginTop: 5, 
        color: Colors.black,
        fontSize: 30
        
    },
    formContainer: {
        ...globalStyles.formContainer,
        marginTop: 50,
        paddingHorizontal: 10
    },
    buttonText:{
        ...globalStyles.buttonText,
         color: Colors.darkGray
     },
    buttonIcon:{
        backgroundColor: Colors.blue,
        height: 50,
        width: 50,
        borderRadius: 25,
        position: 'absolute',
        left: 50,
        bottom: 30,

    },
    icon: {
        textAlign: 'center',
        lineHeight:50,
        color: Colors.white,
    }
})
