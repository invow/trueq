import React, { useContext } from 'react'
import {ScrollView, StyleSheet} from 'react-native'
import {Container, Content, List, Spinner} from 'native-base'
import ItemList from '../components/ItemList'
import Colors from '../constants/Colors';
import { Context } from '../Context'

const ProfilePublications = () => {

    const { myPublications, loading } = useContext(Context)
    
    return (
        <Container>
            <Content> 
                <ScrollView style={styles.tabContent}>
                {loading ? <Spinner style={styles.spinner} color={Colors.red} /> :
                    <List>
                    {
                        myPublications.map((item) => (
                            <ItemList key={item._id} item={item} />
                        ))
                    }
                    </List>}
                </ScrollView>
            </Content>
        </Container>
    )
}

const styles = StyleSheet.create({
    contentContainer: {
		paddingTop: 30,
    }
})

export default ProfilePublications