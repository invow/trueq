import React from 'react';
import { StyleSheet, ScrollView, View, Button, AsyncStorage } from 'react-native';
import { Container, Content, Title, Thumbnail, Tab, Tabs, TabHeading, List, ListItem, Text } from 'native-base';

import RatingStars from '../components/RatingStars';
import ItemCard from '../components/ItemCard';
import Colors from '../constants/Colors';
import ProfileDetailsScreen from './ProfileDetailsScreen'
import ProfilePublications from './ProfilePublications';

export default class ProfileScreen extends React.Component {
	static navigationOptions = {
		header: null,
	};

	render() {
		return (
			<Container>
				<Content contentContainerStyle={styles.content}>
					<Tabs tabBarUnderlineStyle={{ backgroundColor: Colors.red, }}>
						<Tab 
							heading={
								<TabHeading style={{backgroundColor: Colors.white }}>
									<Text style={{color: Colors.black}}>Perfil</Text>
								</TabHeading>
							}
						>
                            <ProfileDetailsScreen navigation={this.props.navigation}> </ProfileDetailsScreen>
						</Tab>
						<Tab
							heading={
								<TabHeading style={{backgroundColor: Colors.white }}>
									<Text style={{color: Colors.black}}>Publicaciones</Text>
								</TabHeading>
							}
						>
							<ProfilePublications navigation={this.props.navigation}> </ProfilePublications>
						</Tab>
					</Tabs>
				</Content>
			</Container>
		);
	}
}

const styles = StyleSheet.create({
	content: {
		top: 18,
		flex: 1,
		position: 'relative',
		alignItems: 'center',
	},
	tabContent: {
		flex: 1,
		marginTop: 10,
		paddingHorizontal: 10,
	},
});
