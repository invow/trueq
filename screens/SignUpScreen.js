import React, { useState, useEffect, useRef, useContext } from 'react';
import { Animated, View, TextInput, StyleSheet, KeyboardAvoidingView, AsyncStorage, TouchableOpacity, Alert } from 'react-native'
import { Content, Text, Spinner } from 'native-base'
import { validateAll } from 'indicative/validator'
import { globalStyles } from '../styles/Global'
import Colors from '../constants/Colors'
import { createUser } from '../services/users'
import { getGeolocation } from '../services/geolocation'
import { Context } from '../Context'


const SignUpScreen = ({navigation}) => {
	const [username, setUsername] = useState('')
	const [email, setEmail] = useState('')
	const [phone, setPhone] = useState('')
	const [location, setLocation] = useState(null)
	const { loading, setLoading } = useContext(Context)
	const opacity = useRef(new Animated.Value(0)).current
	const phoneInput = useRef(null)
	
	useEffect(() => {
		opacityAnim()
	}, [])

	const opacityAnim = () => {
		Animated.timing(opacity, {
			toValue: 1,
			duration: 200,
			useNativeDriver: true
		}).start()
	}
	
	const handleSubmit = async () => {
		setLoading(true)
		try{
			const field_names = {
				username: "nombre",
				email: "email",
				phone: "teléfono",
			}
			const rules = {
				username: 'required|alphaNumeric|min:4',
				email: 'required|email',
				phone: 'required|integer',
			}
			const messages = {
				required: field => `El campo ${field_names[field]} es requerido`,
				'username.min': 'El nombre de usuario debe contener al menos 4 caracteres',
				'email.email': 'Formato de email inválido',
				'phone.integer': 'El teléfono debe contener solo caracteres numéricos',
			}
			
			await validateAll({ username, email, phone }, rules, messages)
			
			const geolocation = await getGeolocation()
			if (geolocation){
				setLocation([geolocation.coords.latitude, geolocation.coords.longitude])
				_createUser()
			}
			
		}catch(err){
			console.log(err)
			let body = ""
			Array.isArray(err) && err.map(e => {body += `• ${e.message} \n`})
			Alert.alert("Verifique los siguientes errores", body)
		}
		setLoading(false)
	}

	const _createUser = async () => {
		const data = {
			username,
			email,
			phone,
			location: {
				type: 'Point',
				coordinates: location
			}
		}
		const response = await createUser(data)
		if (response.status == 201){
			await AsyncStorage.setItem('userToken', response.data.token);
			navigation.navigate('App');
		}
		else{
			response && response.data && response.data.message && alert(response.data.message)
		}
	}
	
	
	const _goLogin = () => {
		navigation.navigate('Auth')
	}
	
	return (
		<Content>
			<KeyboardAvoidingView behavior="padding" style={styles.container}>
			{
				loading ? <Spinner style={styles.spinner} color={Colors.red} /> :
				<>
					<Text style={styles.logo}>
						Trueq
					</Text>
					<Animated.View style={{flex: 1, width: '100%', opacity}}>
						<View style={globalStyles.formContainer}>
							<Text style={styles.termsAgreement}>
								Al registrarse usted acepta los  
								<Text style={styles.signInLink}>
									&nbsp;términos y condiciones de uso
								</Text>
							</Text>
							<TextInput
								autoCapitalize="none"
								returnKeyType="next"
								placeholder="Nombre"
								autoCorrect={false}
								style={globalStyles.textInput}
								placeholderTextColor={styles.placeHolderColor.color}
								onSubmitEditing={() => phoneInput.current.focus()}
								onChangeText={setUsername}
							/>
							<TextInput
								style={globalStyles.textInput}
								autoCapitalize="none"
								keyboardType="email-address"
								returnKeyType="next"
								placeholder="Email"
								autoCorrect={false}
								placeholderTextColor={styles.placeHolderColor.color}
							/>

							<TextInput
								autoCapitalize="none"
								returnKeyType="next"
								placeholder="Teléfono"
								autoCorrect={false}
								style={globalStyles.textInput}
								placeholderTextColor={styles.placeHolderColor.color}
								ref={ phoneInput }
								onChangeText={setPhone}
							/>

							<TouchableOpacity style={globalStyles.button} full onPress={handleSubmit}>
								<Text style={globalStyles.buttonText}>Registrarse</Text>
							</TouchableOpacity>

							<View style={globalStyles.hr}></View>

							<Text style={styles.signIn}>
								<Text onPress={ _goLogin } style={styles.signInLink}>
									YA TENGO CUENTA
								</Text>
							</Text>
						</View>
					</Animated.View>
				</>
			}
			</KeyboardAvoidingView>
		</Content>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		padding: 15,
		backgroundColor: Colors.white,
		width: '100%',
	},
	logo: {
		fontSize: 48,
		paddingLeft: 50,
		paddingRight: 50,
		paddingTop: 50,
		paddingBottom: 15,
		color: Colors.red,
		textAlign: 'center',
		fontFamily: 'lobster-italic',
	},
	signIn: {
		fontSize: 14,
		color: Colors.gray,
	},
	signInLink: {
		fontSize: 14,
		fontWeight: 'bold',
		color: Colors.skin
	},
	placeHolderColor: {
		color: 'rgba(120,120,120,0.7)',
	},
	termsAgreement:{
		marginTop: 0,
		marginBottom:20,
		fontSize: 14,
		color: Colors.gray,
		textAlign:'center'
	},
	spinner: {
		marginTop: '90%'
	}
});

SignUpScreen.navigationOptions = {
	header: null
};

export default SignUpScreen 