import axios from 'axios'

//const apiUrl = 'https://heraldo-api.azurewebsites.net'
const apiUrl = 'https://trueq.azurewebsites.net'

const login = async (payload) => {
    try{
        const data = await axios.post(`${apiUrl}/login`, payload, { validateStatus: () => true })
        return data
    }
    catch(err){
        throw(err)
    }
}

export { login }