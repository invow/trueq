import { AsyncStorage } from 'react-native'
import axios from 'axios'
// const apiUrl = 'http://192.168.1.104:3001'
const apiUrl = 'https://trueq.azurewebsites.net'
const getCategories = async () => {
    const token = await AsyncStorage.getItem('userToken');
        const req = {
        method: 'GET',
        url: `${apiUrl}/categories`,
        headers: {
            'Authorization': `${token}`,
        },
        json: true,
        validateStatus: () => true
    }
    return await axios(req)
}

export { getCategories }