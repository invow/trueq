import * as Location from 'expo-location'
import * as Permissions from 'expo-permissions'

export const getGeolocation = async () => {
    const { status } = await Permissions.askAsync(Permissions.LOCATION);
    
    if (status !== 'granted') {
        Alert.alert("Verifique los siguientes errores", "Debes compartir tu ubicación para crear la cuenta")
    }
    else{
        const gpsServiceStatus = await Location.hasServicesEnabledAsync();
        if (gpsServiceStatus) {
            const location = await Location.getCurrentPositionAsync({});
            return location
        }
        else{
            Alert.alert("Verifique los siguientes errores", "Debes compartir tu ubicación para crear la cuenta")
        }
    }
    return null
}