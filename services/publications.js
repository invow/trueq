import { AsyncStorage } from 'react-native'
import axios from 'axios'

// const apiUrl = 'http://192.168.1.104:3001'
const apiUrl = 'https://trueq.azurewebsites.net'

const createPublication = async (data) => {
    try{
        const token = await AsyncStorage.getItem('userToken');
        let uriParts = data.image.split('.');
        let fileType = uriParts[uriParts.length - 1];

        let formData = new FormData();
        
        formData.append('image', {
            uri: data.image,
            name: `publication-${new Date().getTime()}.${fileType}`,
            type: `image/${fileType}`,
        });
        formData.append('title', data.title)
        formData.append('description', data.description)
        formData.append('category', data.category)
        
        const req = {
            method: 'POST',
            url: `${apiUrl}/publications`,
            data: formData,
            headers: {
                'Authorization': `${token}`,
                'Content-Type': 'multipart/form-data'
            },
            json: true,
            validateStatus: () => true
        }
        return await axios(req)
    }
    catch(err){
        console.log(err)
    }
}

const updatePublication = async (data) => {
    try{
        const token = await AsyncStorage.getItem('userToken');
        let uriParts = data.image.split('.');
        let fileType = uriParts[uriParts.length - 1];

        let formData = new FormData();

        if(data.image) {
            formData.append('image', {
                uri: data.image,
                name: `publication-${new Date().getTime()}.${fileType}`,
                type: `image/${fileType}`,
            });
        }

        if(data.title) {
            formData.append('title', data.title)
        }

        if(data.description) {
            formData.append('description', data.description)
        }

        if(data.category) {
            formData.append('category', data.category)
        }
        
        const req = {
            method: 'PATCH',
            url: `${apiUrl}/publications`,
            data: formData,
            headers: {
                'Authorization': `${token}`,
                'Content-Type': 'multipart/form-data'
            },
            json: true,
            validateStatus: () => true
        }
        return await axios(req)
    }
    catch(err){
        console.log(err)
    }
}

const getMyPublications = async () => {
    const token = await AsyncStorage.getItem('userToken');
    const req = {
        method: 'GET',
        url: `${apiUrl}/publications/mine`,
        headers: {
            'Authorization': `${token}`,
        },
        json: true,
        validateStatus: () => true
    }
    return await axios(req)
}

const getPublications = async ({query, categories, maxDistance, skip, limit}) => {
    const token = await AsyncStorage.getItem('userToken')
    const req = {
        method: 'POST',
        url: `${apiUrl}/get-publications`,
        data: {
            maxDistance,
            categories,
            query
        },
        params: {
            skip,
            limit
        },
        headers: {
            'Authorization': `${token}`,
        },
        json: true,
        validateStatus: () => true
    }
    return await axios(req)
}

const like = async (id) => {
    try{
        const token = await AsyncStorage.getItem('userToken');
        const req = {
            method: 'POST',
            url: `${apiUrl}/like/${id}`,
            headers: {
                'Authorization': `${token}`,
            },
            json: true,
            validateStatus: () => true
        }
        return await axios(req)
    }
    catch(err){
        console.log(err)
    }
}

const deletePublication = async (id) => {
    try{
        const token = await AsyncStorage.getItem('userToken');
        const req = {
            method: 'DELETE',
            url: `${apiUrl}/publications/${id}`,
            headers: {
                'Authorization': `${token}`,
            },
            json: true,
            validateStatus: () => true
        }
        return await axios(req)
    }
    catch(err){
        console.log(err)
    }
}

export { 
    createPublication,
    updatePublication, 
    getPublications,
    getMyPublications, 
    deletePublication,
    like 
}