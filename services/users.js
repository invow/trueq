import axios from 'axios'
import { AsyncStorage } from 'react-native'

// const apiUrl = 'http://192.168.1.104:3001'
const apiUrl = 'https://trueq.azurewebsites.net'

const createUser = async (data) => {
    try{
        return await axios.post(`${apiUrl}/users`, data, { validateStatus: () => true })
    }
    catch(err){
        console.log(err)
    }
}

const updateUser = async (data) => {
    try{
        return await axios.patch(`${apiUrl}/users`, data, { validateStatus: () => true })
    }
    catch(err){
        console.log(err)
    }
}

const getProfile = async () => {
    try {
        const token = await AsyncStorage.getItem('userToken');
        const req = {
            method: 'GET',
            url: `${apiUrl}/users/me`,
            headers: {
                'Authorization': `${token}`
            },
            json: true,
            validateStatus: () => true
        }
        return await axios(req)
    }
    catch(err){
        console.log(err.message)
        throw err
    }
}

const uploadImage = async (data) => {
    try{
        const token = await AsyncStorage.getItem('userToken');
        let uriParts = data.image.split('.');
        let fileType = uriParts[uriParts.length - 1];

        let formData = new FormData();
        
        formData.append('image', {
            uri: data.image,
            name: `profile-${new Date().getTime()}.${fileType}`,
            type: `image/${fileType}`,
        });
        
        const req = {
            method: 'PUT',
            url: `${apiUrl}/users/upload-image`,
            data: formData,
            headers: {
                'Authorization': `${token}`,
                'Content-Type': 'multipart/form-data'
            },
            json: true,
            validateStatus: () => true
        }
        return await axios(req)
    }
    catch(err){
        console.log(err)
        throw err
    }
}

const getMatches = async () => {
    try{
        const token = await AsyncStorage.getItem('userToken')
        const req = {
            method: 'GET',
            url: `${apiUrl}/matches`,
            headers: {
                'Authorization': `${token}`
            },
            json: true,
            validateStatus: () => true
        }
        return await axios(req)
    }
    catch(err){
        console.log(err)
        throw err
    }
}

export { createUser, updateUser, getProfile, uploadImage, getMatches }