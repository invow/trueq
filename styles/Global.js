import { StyleSheet } from 'react-native'
import Colors from '../constants/Colors'

const input = {
	height: 40,
	marginBottom: 15,
	padding: 10,
	width: '100%',
	borderRadius: 2,
	shadowColor: "#000",
	shadowOffset: {
		width: 0,
		height: 3,
	},
	shadowOpacity: 0.1,
	shadowRadius: 2.00,
	elevation: 1,
}

export const globalStyles = StyleSheet.create({
	textInput: {
		...input,
		borderRadius: 25,
		backgroundColor: Colors.lightGray,
	},
	button: {
		...input,
		padding: 8,
		borderRadius: 25,
		backgroundColor: Colors.red,
	},
	buttonText: {
		color: '#fff',
		textAlign: 'center',
		fontWeight: '700',
	},
	buttonIcon: {
		color: '#fff',
	},
	hr: {
		height: 0.,
		width: '100%',
		marginTop: 10,
		marginBottom: 10,
		padding: 0,
		backgroundColor: Colors.lightGray,
		borderColor: Colors.lightGray2,
	},
	h100:{
		height: '100%'
	},
	formContainer: {
		flex: 1,
		position: 'relative',
		alignItems: 'center',
	}
})